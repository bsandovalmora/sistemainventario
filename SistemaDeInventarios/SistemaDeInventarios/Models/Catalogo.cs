﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaDeInventarios.Models
{
    public class Catalogo
    {
        public int id { get; set; }                        

        [StringLength(20)]
        public string estado { get; set; }
        
        public List<Articulos> Articulos { get; set; }


    }
}