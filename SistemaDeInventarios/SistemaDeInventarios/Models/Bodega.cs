﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaDeInventarios.Models
{
    public class Bodega
    {
        public int id { get; set; }

        [StringLength(20)]
        [Required]
        public string nombre { get; set; }

        public int capacidadMAX { get; set; }
        public int capacidadDisponible { get; set; }
        public List<Articulos> Articulos { get; set; }
    }
}