﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SistemaDeInventarios.Models
{
    public class Articulos
    {
        public int id { get; set; }

        [Required]
        [StringLength(20)]
        public string nombre { get; set; }

        [StringLength(100)]
        public string descripcion { get; set; }

        public int precioVenta { get; set; }                

        public int cantidad { get; set; }

        public int bodegaId { get; set; }
        [ForeignKey("bodegaId")]
        public Bodega bodega { get; set; }

        public int catalogoId { get; set; }
        [ForeignKey("catalogoId")]
        public Catalogo catalogo { get; set; }

    }
}