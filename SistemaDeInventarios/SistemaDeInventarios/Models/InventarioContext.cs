﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SistemaDeInventarios.Models
{
    public class InventarioContext : DbContext
    {
        public InventarioContext() : base("connDB")
        {

        }

        public DbSet<Usuarios> Usuarios { get; set; }
        public DbSet<Catalogo> Catalogos { get; set; }

        public DbSet<Articulos> Articulos { get; set; }
        public DbSet<Bodega> Bodegas { get; set; }

    }
}