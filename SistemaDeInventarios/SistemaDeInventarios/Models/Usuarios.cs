﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaDeInventarios.Models
{
    public class Usuarios
    {        
        
        public int id { get; set; }

        [Required]
        [StringLength(20)]
        public string username { get; set; }

        [Required]
        [StringLength(15)]
        public string pass { get; set; }

        [Required]
        [StringLength(10)]
        public string rol { get; set; }

    }
}