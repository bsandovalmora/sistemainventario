﻿using SistemaDeInventarios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaDeInventarios.Controllers
{
    public class HomeController : Controller
    {

        private Metodos _m;

        public HomeController()
        {
            _m = new Metodos();
        }


        
        public ActionResult Index()
        {
            //Verifica la existencia del usuario y de su rol 'ADMIN'

            //mensaje para notificar eventos
            if (Session["Mensaje"] != null)
            {
                ViewBag.Message = Session["Mensaje"].ToString();
                Session["Mensaje"] = "";
            }

            //verifica si la solicitud es null
            if (Request.Form["username"] != null)
            {
                string mjs = _m.VerificarInicioSession(Request.Form["username"].ToString(), Request.Form["pass"].ToString());
                if (mjs.Equals("admin")) {
                    Session["username"] = Request.Form["username"].ToString();
                    Session["username"] = mjs;                    
                }
                else
                {
                    Session["MessageLogin"] = "Usuario ó contraseña incorrecta";
                }
            }

            if (Session["rol"] == null && Session["username"] == null)
                    return RedirectToAction("Login", "Home");


            //solicitudes de lista de los articulos, bodegas y catalogos 

            var mostrar = _m.ObtenerArticulos();
            ViewBag.tipoCatalogo = new SelectList(_m.ObtenerCatalogo(), "id", "estado");
            ViewBag.Bodega = new SelectList(_m.ObtenerBodegas(), "id", "nombre");
            ViewBag.ListaBodega = _m.ObtenerBodegas();

            return View(mostrar);
        }

        public ActionResult Login()
        {
            if (Session["MessageLogin"] != null) {

                ViewBag.Login = Session["MessageLogin"].ToString();

                Session["MessageLogin"] = "";
            }
                


            return View();
        }

        public ActionResult Prueba()
        {
            return View();
        }


        //crear articulo, se llama los parametros necesarios para guardar un nuevo articulo
        //se instancia una clase Articulos para enviarla como parametro al metodo Crear en la clase Metodos
        [HttpPost]
        public ActionResult Create()
        {
            if (Session["rol"] == null && Session["username"] == null)
                return RedirectToAction("Login", "Home");

            string nombre = Request.Form["nombre"].ToString();
            string descripcion = Request.Form["descr"].ToString();
            int precioventa = Convert.ToInt32(Request.Form["precioVenta"].ToString());
            int cantidad = Convert.ToInt32(Request.Form["cantidad"].ToString());
            int bodegaId = Convert.ToInt32(Request.Form["BodegaId"].ToString());
            int catalogoId = Convert.ToInt32(Request.Form["CatalogoId"].ToString());

            Articulos a = new Articulos();
            a.nombre = nombre;
            a.descripcion = descripcion;
            a.precioVenta = precioventa;
            a.cantidad = cantidad;
            a.bodegaId = bodegaId;
            a.catalogoId = catalogoId;

            string mjs = _m.Crear(a);
            string mjs1 = _m.ActualizarBodegaCapacidad(bodegaId,cantidad);

            if (mjs.Equals("error") && mjs1.Equals("error"))
                Session["Mensaje"] = "Error al guardar artículo";
            else
                Session["Mensaje"] = "Artículo guardado | Capacidad de bodega cod # "+ bodegaId + " disminuida";

            return RedirectToAction("Index","Home");
        }


        //Elimina articulos por id, recibe como respuesta un string para verificar el proceso del metodo por si fallo o fue un exito
        public ActionResult Delete(int id)
        {
            string mjs = _m.Eliminar(id);

            if(mjs.Equals("error"))
                Session["Mensaje"] = "Error al eliminar artículo";
            else
                Session["Mensaje"] = "Artículo eliminado";

            return RedirectToAction("Index", "Home");
        }


        //Actualizar articulos por id, recibe como respuesta un string para verificar el proceso del metodo por si fallo o fue un exito
        public ActionResult Edit()
        {
            if (Session["rol"] == null && Session["username"] == null)
                return RedirectToAction("Login", "Home");

            int id = Convert.ToInt32(Request.Form["edit_id"].ToString());
            string nombre = Request.Form["edit_nombre"].ToString();
            string descripcion = Request.Form["edit_descr"].ToString();
            int precioventa = Convert.ToInt32(Request.Form["edit_precioVenta"].ToString());
            int cantidad = Convert.ToInt32(Request.Form["edit_cantidad"].ToString());
            int bodegaId = Convert.ToInt32(Request.Form["edit_BodegaId"].ToString());
            int catalogoId = Convert.ToInt32(Request.Form["edit_CatalogoId"].ToString());

            Articulos a = new Articulos();
            a.id = id;
            a.nombre = nombre;
            a.descripcion = descripcion;
            a.precioVenta = precioventa;
            a.cantidad = cantidad;
            a.bodegaId = bodegaId;
            a.catalogoId = catalogoId;

            string mjs = _m.Editar(a);

            if (mjs.Equals("Editado"))
                Session["Mensaje"] = "Artículo modificado";
            else
                Session["Mensaje"] = "Error al eliminar artículo";


            return RedirectToAction("Index", "Home");
        }
    }
}