﻿namespace SistemaDeInventarios.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SistemaDeInventarios.Models.InventarioContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(SistemaDeInventarios.Models.InventarioContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            context.Catalogos.AddOrUpdate(x => x.estado, new Models.Catalogo()
            {
                estado = "Almacenado"
            }, new Models.Catalogo()
            {
                estado = "En traslado"
            }, new Models.Catalogo()
            {
                estado = "Vendido"
            });

            context.Bodegas.AddOrUpdate(b => b.nombre, new Models.Bodega()
            {
                nombre = "Bodega 1",
                capacidadMAX = 40,
                capacidadDisponible = 40

            }, new Models.Bodega()
            {
                nombre = "Bodega 2",
                capacidadMAX = 40,
                capacidadDisponible = 40
            }, new Models.Bodega()
            {
                nombre = "Bodega 3",
                capacidadMAX = 40,
                capacidadDisponible = 40
            });

            

            context.Usuarios.AddOrUpdate(u => u.username, new Models.Usuarios()
            {
                username = "admin",
                pass = "2020@Fg",
                rol = "admin"
            });
        }
    }
}
