﻿$(document).ready(function () {
    $("#message")
    setTimeout(function () {
        $("#message").hide();
    }, 7000);      

    Filter();
});

function RellarModal(id) {
    var tr = document.getElementById("fila " + id);

    var id = tr.getElementsByTagName("td")[0].innerHTML.trim();
    var nombre = tr.getElementsByTagName("td")[1].innerHTML.trim();
    var desp = tr.getElementsByTagName("td")[2].innerHTML.trim();
    var precio = tr.getElementsByTagName("td")[3].innerHTML.trim();
    var cantidad = tr.getElementsByTagName("td")[4].innerHTML.trim();
    var estado = tr.getElementsByTagName("td")[5].innerHTML.trim();
    var bodega = tr.getElementsByTagName("td")[6].innerHTML.trim();

    var valueEstado;
    var select_estado = document.getElementById("edit_CatalogoId");
    var itemestado = select_estado.getElementsByTagName("option");

    var valuBodega;
    var select_Bodega = document.getElementById("edit_BodegaId");
    var itemBodega = select_Bodega.getElementsByTagName("option");

    for (var i = 0; i < itemestado.length; i++) {        
        if (estado == itemestado[i].innerHTML.trim()) {
            valueEstado = itemestado[i].value;
        }
    }

    for (var e = 0; e < itemBodega.length; e++) {
        if (bodega == itemBodega[e].innerHTML.trim()) {
            valuBodega = itemBodega[e].value;
        }
    }
    $("#edit_id").val(id);
    $("#edit_nombre").val(nombre);
    $("#edit_descr").val(desp);
    $("#edit_precioVenta").val(precio);
    $("#edit_cantidad").val(cantidad);
    $("#edit_CatalogoId").val(valueEstado);
    $("#edit_BodegaId").val(valuBodega);
    
}

function Filter() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("btnfiter");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabla-articulos");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        td1 = tr[i].getElementsByTagName("td")[1];
        if (td || td1) {
            txtValue = td.textContent || td.innerText;
            txtValue1 = td1.textContent || td1.innerText;

            if (txtValue.toUpperCase().indexOf(filter) > -1 || txtValue1.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}