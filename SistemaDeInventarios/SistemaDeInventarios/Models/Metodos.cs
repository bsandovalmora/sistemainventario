﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace SistemaDeInventarios.Models
{
    public class Metodos
    {
        public List<Catalogo> ObtenerCatalogo()
        {
            using (var db = new InventarioContext())
            {
                return db.Catalogos.ToList();
            }
        }

        public List<Bodega> ObtenerBodegas()
        {
            using (var db = new InventarioContext())
            {
                return db.Bodegas.ToList();
            }
        }

        public List<Articulos> ObtenerArticulos()
        {
            using ( var db = new InventarioContext())
            {                
                return db.Articulos.Include(x => x.catalogo).Include(x => x.bodega).ToList();                
            }
        }

        public string Crear(Articulos a)
        {
            string mjs = "";

            try
            {
                using (var db = new InventarioContext())
                {
                    db.Articulos.Add(a);
                    db.SaveChanges();
                    mjs = "Guardado";
                }
            }
            catch (Exception e)
            {
                mjs = "error";
            }

            return mjs;
        }

        public string Editar(Articulos a)
        {
            string mjs = "";

            try
            {
                using (var db = new InventarioContext())
                {
                    db.Articulos.AddOrUpdate(x => x.id, a);
                    db.SaveChanges();
                    mjs = "Editado";
                }
            }
            catch (Exception e)
            {
                mjs = "error";
            }

            return mjs;
        }


        //buscar la entidad respectiva por medio del id y del metodo Find donde se devuelve un objecto Articulo
        //y se procede a remover el objecto de la base de datos
        public string Eliminar(int id)
        {
            string mjs = "";

            try
            {
                using (var db = new InventarioContext())
                {
                    var a = db.Articulos.Find(id);
                    db.Articulos.Remove(a);
                    db.SaveChanges();
                    mjs = "Eliminado";
                }
            }
            catch (Exception e)
            {
                mjs = "error";
            }

            return mjs;
        }

        //Verifica la existencia del usuario en la base de datos y retorna un mensaje si este usuario existe o no
        public string VerificarInicioSession(string username, string pass)
        {
            string mjs = "";

            try
            {
                using (var db = new InventarioContext())
                {
                    var u = db.Usuarios.Where(us => us.username == username && us.pass == pass).Select(us => new { rol = us.rol});

                    foreach (var obj in u)
                    {
                        mjs = obj.rol;
                        return mjs;
                    }                    
                }
            }
            catch (Exception e)
            {
                mjs = "Error";
            }
            return mjs;
        }


        //al agregar un articulo se actualiza la capacidad de la bodega
        public string ActualizarBodegaCapacidad(int id, int cantidad)
        {
            string mjs = "";

            try
            {
                using (var db = new InventarioContext())
                {
                    var b = db.Bodegas.Find(id);
                    b.capacidadDisponible = b.capacidadMAX - cantidad;

                    db.Bodegas.AddOrUpdate(x => x.id, b);
                    db.SaveChanges();
                    mjs = "Actualizado";
                }
            }
            catch (Exception e)
            {
                mjs = "error";
            }

            return mjs;
        }

    }
}